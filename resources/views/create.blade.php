@extends('master');
@section('content')

<!-- Default horizontal form -->
<form action="{{ Route('store') }}" method="post" class="mt-5" style="max-width: 900px;margin: 0 auto" data-parsley-validate>
    {{ csrf_field() }}

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <h2>Add Student</h2><hr>
    <!-- Grid row -->
    <div class="form-group row">
        <!-- Default input -->
        <label for="name" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control" id="name" placeholder="Name" required>
        </div>
    </div>
    <!-- Grid row -->


    <!-- Grid row -->
    <div class="form-group row">
        <!-- Default input -->
        <label for="reg_id" class="col-sm-2 col-form-label">Registration ID</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="registration_id" id="reg_id" placeholder="Registration ID" required>
        </div>
    </div>
    <!-- Grid row -->

    <!-- Grid row -->
    <div class="form-group row">
        <!-- Default input -->
        <label for="department_name" class="col-sm-2 col-form-label">Department Name</label>
        <div class="col-sm-10">
            <input type="text" name="department_name" class="form-control" id="department_name" placeholder="Department Name" required>
        </div>
    </div>
    <!-- Grid row -->

    <!-- Grid row -->
    <div class="form-group row">
        <!-- Default input -->
        <label for="info" class="col-sm-2 col-form-label">	Info</label>
        <div class="col-sm-10">
            <input type="text" name="info" class="form-control" id="info" placeholder="Information">
        </div>
    </div>
    <!-- Grid row -->

    <!-- Grid row -->
    <div class="form-group row">
        <label  class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
            <button type="submit" name="submit" class="btn btn-primary btn-md">Submit</button>
        </div>
    </div>
    <!-- Grid row -->
</form>
<!-- Default horizontal form -->
@endsection