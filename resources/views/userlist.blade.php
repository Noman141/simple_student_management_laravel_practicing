@extends('master')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Email</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        @foreach($users as $user)

        <tr>
            <th scope="row">{{ $user->id }}</th>
            <td>{{ $user->email }}</td>
            <td>
                <form action="{{ route('send_email') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <input type="submit" class="btn btn-info" value="Send Email">
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    @auth
    <h2> Custom Message and Custom Style</h2>
    <form action="{{ route('send_custom') }}" method="post">
        {{ csrf_field() }}

        <!-- Default horizontal form -->
        <!-- Grid row -->
        <div class="form-group row">
            <!-- Default input -->
            <label for="address" class="col-sm-2 col-form-label">Shipping Address</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="shipping_address" id="address" placeholder="Shipping Address">
            </div>
        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="form-group row">
            <!-- Default input -->
            <label for="phone_number" class="col-sm-2 col-form-label">Phone Number</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number">
            </div>
        </div>
        <!-- Grid row -->
            <!-- Grid row -->
            <div class="form-group row">
                <!-- Default input -->
                <label for="price" class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" name="price" id="price" placeholder=" Price">
                </div>
            </div>
            <!-- Grid row -->

        <!-- Grid row -->
        <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary btn-md">Confirm</button>
            </div>
        </div>
        <!-- Grid row -->

            <!-- Default horizontal form -->
    </form>
    @endauth

@endsection