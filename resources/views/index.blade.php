@extends('master')
@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th width="5%">#</th>
            <th width="20%%">Name</th>
            <th width="10%">Registration</th>
            <th width="10%">Department</th>
            <th width="30%">Info</th>
            <th width="25%%">Action</th>
        </tr>
        </thead>
        @php $i = 0;@endphp
        @foreach($students as $key => $student)
            @php $i++;@endphp
            <tbody>
            <tr>
                <th scope="row">{{ $i }}</th>
                <td>{{ $student->name }}</td>
                <td>{{ $student->registration_id }}</td>
                <td>{{ $student->department_name }}</td>
                <td>{{ $student->info }}</td>
                <td>
                    <a href="{{ Route('edit',$student->id) }}" class="btn btn-info">Edit</a>
                    <a href="{{ Route('delete',$student->id) }}" class="btn btn-danger">Delete</a>
                </td>
            </tr>
            </tbody>
        @endforeach
    </table>
@endsection



