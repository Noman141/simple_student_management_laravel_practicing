<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PagesController extends Controller{
    //

    public function index(){
        $posts = Post::orderBy('id','desc')->get();
        return view('index')->withPosts($posts);
    }
}
